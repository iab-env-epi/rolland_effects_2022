# Effects of early exposure to phthalates on cognitive development and visual behavior at 24 months

Git repo for ([Rolland et al., 2022](https://www.sciencedirect.com/science/article/abs/pii/S0013935122023957?via%3Dihub))

## Analysis overview

This analysis was performed using the {[drake](https://github.com/ropensci/drake)} workflow management package.

To run the analysis only the `makefile.R` script needs to be executed. This makefile calls the drake analysis plan (`R/analysis_plan.R`) which outlines the successive analysis steps and calls the different functions used to execute different analysis steps, to be found in the `R/` folder. 

Re-running the analysis also requires an additional `data/` folder not shared here, containing the different data sets presented hereafter. These data can only be provided upon request.

### data/ folder

Analysis input data-files are not made available as they contain sensitive information. The analysis input data files are:

* `babylab_210106.dta` = Eye tracking measures provided by the BabyLab research lab ([http://www.babylab-grenoble.fr/](http://www.babylab-grenoble.fr/)), 
* `crb_clean_2021-05-21.csv` = data from Grenoble's Biological resource center (Centre de Ressources Biologiques (CRB)), sample specific info such as sampling date, specific gravity, etc 
* `phenols_phthalates_pregnancy_mr_2021-03-19_352.csv` = urine samples measurements of phtalates from the Norwegian Institute of Public Health (NIPH), containing concentrations for each pregnant mother at the second and third trimesters of pregnancy
* `phenols_phthalates_infancy_mr_544_2021-09-15.csv` = urine samples measurements of phtalates from the Norwegian Institute of Public Health (NIPH), containing concentrations for each child at 12 months
* `bdd_grossesse_v4_1.dta` = Sepages data containing individual pregnancy related info (mom age, BMI, smoking etc)
* `mohadgrt3_ag_20201214_31.csv` = Hospital Anxiety and Depression (HAD) scores measured in mothers during pregnancy at the 3rd trimester
* `breastfeeding_2021-06-10.csv` = Information regarding breastfeeding duration
* `limits_data_2019-12-09.csv` = LOD and LOQ for the different phtalates
* `extract_cy3cak1_q04p4a_210924.csv` = Information regarding maternal IQ

### results/ folder

Results files in the `results/` folder:

* `main_results_2022-09-18.html` = all article results including sensitivity analyses and supplementary data
* `model_diagnostics.html` = diagnostics for all paper models

Code used to produce these files is found in the corresponding .Rmd files.

### R/ folder

This folder contains all the functions used for the analysis, called in the `makefile.R` script.

* `analysis_plan.R` = drake plan outlying the different steps of the analysis and setting the dependencies
* `data_preparation.R` = all functions relative to data preparation except occupation and breastfeeding (each very long and seperated)
* `breastfeedin_data_preparation.R` = preparation of the breastfeeding data
* `packages.R` = analysis packages
* `descriptive_analysis.R` = functions used to produce descriptive results (tables, correlations, etc)
* `stat_models.R` = multivariate models
* `tidy_results.R` = prepare model outputs to publication format

## Session info

```
R version 4.2.0 (2022-04-22 ucrt)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 10 x64 (build 19044)

Matrix products: default

locale:
[1] LC_COLLATE=French_France.utf8  LC_CTYPE=French_France.utf8   
[3] LC_MONETARY=French_France.utf8 LC_NUMERIC=C                  
[5] LC_TIME=French_France.utf8    

attached base packages:
[1] splines   stats     graphics  grDevices utils     datasets  methods  
[8] base     

other attached packages:
 [1] fastDummies_1.6.3 bkmr_0.2.2        betareg_3.1-4     ggh4x_0.2.1      
 [5] lmtest_0.9-40     zoo_1.8-10        see_0.7.0         performance_0.9.0
 [9] parameters_0.18.0 lme4_1.1-29       Matrix_1.4-1      car_3.0-13       
[13] carData_3.0-5     lubridate_1.8.0   haven_2.5.0       forcats_0.5.1    
[17] stringr_1.4.0     dplyr_1.0.9       purrr_0.3.4       readr_2.1.2      
[21] tidyr_1.2.0       tibble_3.1.7      ggplot2_3.3.6     tidyverse_1.3.1  
[25] drake_7.13.3     

loaded via a namespace (and not attached):
 [1] nlme_3.1-157      fs_1.5.2          insight_0.17.1    filelock_1.0.2   
 [5] progress_1.2.2    httr_1.4.3        tools_4.2.0       backports_1.4.1  
 [9] tmvtnorm_1.5      utf8_1.2.2        R6_2.5.1          DBI_1.1.2        
[13] colorspace_2.0-3  nnet_7.3-17       withr_2.5.0       tidyselect_1.1.2 
[17] prettyunits_1.1.1 compiler_4.2.0    cli_3.3.0         rvest_1.0.2      
[21] xml2_1.3.3        sandwich_3.0-1    bayestestR_0.12.1 scales_1.2.0     
[25] mvtnorm_1.1-3     digest_0.6.29     txtq_0.2.4        minqa_1.2.4      
[29] rmarkdown_2.14    pkgconfig_2.0.3   htmltools_0.5.2   dbplyr_2.1.1     
[33] fastmap_1.1.0     rlang_1.0.2       readxl_1.4.0      rstudioapi_0.13  
[37] generics_0.1.2    jsonlite_1.8.0    magrittr_2.0.3    modeltools_0.2-23
[41] Formula_1.2-4     Rcpp_1.0.8.3      munsell_0.5.0     fansi_1.0.3      
[45] abind_1.4-5       lifecycle_1.0.1   stringi_1.7.6     MASS_7.3-56      
[49] storr_1.2.5       flexmix_2.3-17    grid_4.2.0        parallel_4.2.0   
[53] crayon_1.5.1      lattice_0.20-45   hms_1.1.1         knitr_1.39       
[57] pillar_1.7.0      igraph_1.3.1      boot_1.3-28       base64url_1.4    
[61] stats4_4.2.0      reprex_2.0.1      glue_1.6.2        evaluate_0.15    
[65] modelr_0.1.8      vctrs_0.4.1       nloptr_2.0.1      tzdb_0.3.0       
[69] cellranger_1.1.0  gtable_0.3.0      assertthat_0.2.1  datawizard_0.4.1 
[73] xfun_0.30         broom_0.8.0       gmm_1.6-6         ellipsis_0.3.2  
```

## Article info

[Link to article]()


