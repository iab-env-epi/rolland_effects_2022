# Statistical analysis part of the babylab analysis
# M. Rolland
# 08/01/2020

# Functions to do with the statistical modelling process:
# * getting the model formula
# * computing the appropriate model
# * extracting the appropriate information (model, pval, etc)

# Model formula ----
# function to produce model formula for the different modelling steps of the 
# analysis, inputs are a list of covariates and a string to choose the model;
# returns a model formula
get_formula <- function(covars, which){
  if(which == "no_exp"){
    form <- paste(covars, collapse = " + ") %>% 
      str_c("score ~ ", .) %>%
      str_replace(., "expe_h", "ns(expe_h, 4)")
  } else if(which == "no_exp2"){
    form <- paste(covars, collapse = " + ") %>% 
      str_c("score ~ ", .)
  } else if(which == "exp"){
    form <- paste(covars, collapse = " + ") %>%
      str_c("score ~ log(exp_val) + ", .)  # add child age interaction
  } else if(which == "sex"){
    form <- paste(covars, collapse = " + ") %>%
      str_c("score ~ log(exp_val) + ", ., "+ sex * log(exp_val)")  # add child age interaction
  } else if(which == "unadjusted"){
    form <- "score ~ log(exp_val)" 
  } else if(which == "sg"){
    form <- paste(covars, collapse = " + ") %>%
      str_c("score ~ log(exp_val) + sg + ", .)  
  }
  return(form)
}

# Models without exposure ----
# run models without exposure
get_models_no_exp <- function(data, covars, which){
  # set model formula
  form <- get_formula(covars, which)
  
  # run model for each outcome
  models <- data %>%
    group_by(outcome) %>%
    group_map(
      ~linear_model(.x, .y, form)
    )
  
  return(models)
}

# Get p for models without exposure
get_pval_no_exp <- function(model_data, covars, which){
  # get formula for the models without exposure
  form <- get_formula(covars, which)
  
  # run model for each outcome
  anova_tab <- model_data %>%
    group_by(outcome) %>%
    group_modify(
      ~tidy_anova(lm(as.formula(form), data = .))
    )
  
  return(anova_tab)
}

# Linear models ----
# Produce all models with call to the linear_model function for each set of 
# parameters and formula
# * model data : input data in long format with all exposure data, covariates, outcome
# * covars     : list of covariates to be included in the model
# * which      : string to say which model to run 
#                   "exp"        = models with exposure, 
#                   "sex"        = models with sex interaction, 
#                   "unadjusted" = model with no covariate,
#                   "no_exp"     = no exposure, experiment hour categorical
#                   "no_exp2"    = no exposure, experiment hour as spline
#                   "sg"         = adjusted for SG
# Basically a call for the linear_model function which runs a model for each 
# outcome, each compoiund and each exposure time 
get_models <- function(model_data, covars, which){
  
  # get model formula
  form <- get_formula(covars, which)
  
  # run model for each outcome and each exposure time
  final_models <- model_data %>%
    mutate(
      exp_val = as.numeric(exp_val)
    ) %>%
    group_by(outcome, compound, exp_t) %>%
    group_map(
      ~linear_model(.x, .y, form)
    )
  
  return(final_models)
}

# Single linear model for a given set of parameters and a given formula
linear_model <- function(model_data, params, form){
  print(params)
  if(! params$outcome %in% c("pct_eyes", "novelty")){
    # compute model
    model <- lm(as.formula(form), data = model_data)
  } else {
    # beta regression for pct eyes and novelty (which are percents)
    model <- betareg(as.formula(form), data = model_data)
  }
  
  # identify outliers
  outliers <- model %>%
    check_outliers(., 
                   method = "cook", 
                   threshold = list('cook' = 0.026)) %>% # 4/150 ~ 0.026, recommended to use N / 4 as threshold for cook
    as.data.frame()
  
  # list outliers
  outlier_list <- as.numeric(outliers$Obs[outliers$Outlier == 1])
  
  # remove and rerun model if any outliers else keep model with all data
  if(length(outlier_list) > 0 & !params$outcome %in% c("pct_eyes", "novelty")){
    model_data   <- model_data %>% slice(-outlier_list)
    if(!params$outcome %in% c("pct_eyes", "novelty")){
      model_no_out <- lm(as.formula(form), data = model_data) 
    } else {
      print(head(model_data))
      # beta regression for pct eyes
      model_no_out <- betareg(as.formula(form), data = model_data)
    }
  } else {
    model_no_out <- model
  }
  
  # outlier function (performance::check_outliers) does not work for beta regression
  if(params$outcome %in% c("pct_eyes")){outlier_list = NULL}
  
  # save output alongside with variabe in single object
  model_obj <- list(
    outcome    = params, 
    fit        = model, 
    n_outliers = length(outlier_list),
    fit_no_out = model_no_out
  )
  
  return(model_obj)
}

