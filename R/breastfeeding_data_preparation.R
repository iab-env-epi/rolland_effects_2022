# Preparation of the breastfeeding variable
# M. Rolland
# 04/05/2021

# e-mail Sarah 04/05/2021

# "la base envoyée par Karine en date du 05.12.2018 doit être mise à jour car il 
# y a eu quelques réponses supplémentaires pour le questionnaire CY1EBB2 
# (allaitement - 12 mois) et le questionnaire de rattrapage (CY1LAB1). Pour 
# cela, Matthieu tu peux contacter Anne en me mettant en copie

# il n'y a pas d'autre questionnaire sur les durées d'allaitement. Si a un an 
# l’enfant est encore allaité nous ne pourrons pas connaitre  la date de fin de 
# l’allaitement. Nous pouvons simplement dire qu'a la date de réponse du 
# questionnaire CY1EBB2 l'enfant est encore allaité (je me souviens d'une 
# discussion en COPIL ou nous avions dit que l’intérêt est de connaitre la durée  
# d'allaitement exclusif mais pas la durée total d'allaitement mixte). Nous 
# avons cependant :

# * le questionnaire à 18 mois qui demande à la volontaire si l'enfant est 
#   encore allaité (questionnaire CY2EAQ9, voir ci joint)  : la réponse est 
#   oui/non il n'y a pas de date de fin d'allaitement.
# * le questionnaire à 36 mois qui demande à la volontaire la consommation de 
#   lait depuis les deux ans de l'enfant (lait de croissance, de vache et autre) 
#   --> c'est les questions Q35 a Q37 du questionnaire CY3EBE1 que je mets 
#   également en pièce jointe"

# milk data preparation: breastfeeding/formula status
prepare_bf_data <- function(bf_data_raw, sepages){
  # Set maximum age for continuous breasfeeding duration to the start of the
  # filling of the 12 months questionnaire (ie 48 weeks) 
  # Email Sarah 11/05/21 
  # "le questionnaire CY1EBB2 est un auto-questionnaire que les volontaires
  # devaient remplir en ligne. Les questionnaires ont effectivement été ouverts
  # un mois avant les un an (soit 48 semaines) et si le questionnaire n'était
  # pas rempli au moment de la visite à l'HCE, l’enquêteur a administré ce
  # questionnaire au cours de la visite (d’où le nombre de réponses élevées...)"
  n_weeks <- 48
  
  # Birth dates
  bd_data <- sepages %>% 
    select(ident, po_datedel) %>%
    mutate(
      po_datedel = ymd(po_datedel)
    )
  
  # Add dates to breastfeeding
  bf_data <- bf_data_raw %>%
    left_join(bd_data, by = "ident")
  
  # Manual correction----
  # Export all questions where two age categories were filled
  # source("data/data_preparation/bf_formula_two_age_cat_filled_instead_of_one.R")
  # At 17/12/2018
  # 48 cases
  # Yoann said that they may have entered 2 months and 2 weeks instead of 6 weeks
  # Only one remaining case that seems inconsistent: 
  # id = 22368 filled 2 months and 5 weeks for the breastfeeding end date
  # She also indicates starting formula at 5 months and 1 week
  # There probably was an inversion between week and month of breastfeeding end
  bf_data$cy1ebb2_q01p1[bf_data$ident == "22368"] <- 5
  bf_data$cy1ebb2_q01p2[bf_data$ident == "22368"] <- 2
  
  # Incorrect BF end date 2001 instead of 2017 (decided with Claire and Sarah on
  # 10/05/21 on ZOOM)
  year(bf_data$cy1ebb2_q01p3[bf_data$ident == "23371"]) <- 2017
  
  # Data pre-processing ----
  # Add 1 unit when end age given in days/weeks/months
  # NOTE: 1 unit is added when breastfeeding or formula end age given in weeks/months to include 
  # the whole period. Eg if child born on 01/01/N and breasfeeding stopped at "12 months" we 
  # decided that breastfeeding stop date == 01/02/N+1 and not 01/01/N+1 because we don't know 
  # when in the 12th month the breastfeeding actually stopped. Not doing so caused us to be too 
  # restrictive
  bf_data <- bf_data %>%
    mutate(
      cy1ebb1_q01p1 = cy1ebb1_q01p1 + 1,
      cy1ebb1_q01p2 = cy1ebb1_q01p2 + 1,
      cy1ebb1_q02p6 = cy1ebb1_q02p6 + 1,
      cy1ebb1_q02p7 = cy1ebb1_q02p7 + 1,
      cy1ebb2_q01p1 = cy1ebb2_q01p1 + 1,
      cy1ebb2_q01p2 = cy1ebb2_q01p2 + 1,
      cy1ebb2_q02p6 = cy1ebb2_q02p6 + 1,
      cy1ebb2_q02p7 = cy1ebb2_q02p7 + 1
    )
  
  # For age elements where one of week/month is completed and the other missing 
  # NAs are set to 0 in order to be summed in the subsequent formulas
  bf_data <- bf_data %>%
    mutate(
      cy1ebb1_q01p1 = ifelse(!is.na(cy1ebb1_q01p2) & is.na(cy1ebb1_q01p1), 0, cy1ebb1_q01p1),
      cy1ebb1_q01p2 = ifelse(!is.na(cy1ebb1_q01p1) & is.na(cy1ebb1_q01p2), 0, cy1ebb1_q01p2),
      cy1ebb2_q01p1 = ifelse(!is.na(cy1ebb2_q01p2) & is.na(cy1ebb2_q01p1), 0, cy1ebb2_q01p1),
      cy1ebb2_q01p2 = ifelse(!is.na(cy1ebb2_q01p1) & is.na(cy1ebb2_q01p2), 0, cy1ebb2_q01p2)
    )
  
  # Breastfeeding duration in the first N months ----
  # Here we compute the durationn of breasfeeding in weeks in the first 13 
  # months
  # Each observation goes through steps 1 to 6 until an end of breastfeeding is 
  # computed. 
  # Step0: initiate breastfeeding (BF) start date to date of delivery and date 
  #        of end of BF to missing
  # Step1: we look at ever breastfed yes/no at the first (2 month) questionnaire
  #        => If never breastfed: breastfeeding end date = breastfeeding start 
  #          date
  # Step2: we look at the 2 and 12 month end of BF date. In cases when both a 
  #        date and an age of BF was given, we gave priority to the date. 
  # Step3: we look at the 2 and 12 month end of BF age. When month and week age 
  #        were provided it was considered age end = month + week
  # Step4: we look at the 12 month questionnaire, still breasfted variable
  #        => if still breastfed then BF end date = BF start date + 13 months 
  #        AND a still breastfed at 13 month flag
  # Step5: we look at the catchup questionnaire still breastfed question 
  #        * If baby was breastfed non exclusively and age of end of BF missing
  #          then baby was considered still breastfed at 12 months because this
  #          questionnaire was administered on the phone and low probability of 
  #          mistake 
  #          => date end BF = BF start date + 13 months AND still breastfed at 
  #             13 months flag
  #        * Else if baby was breastfed exclusively and age of end of BF missing, 
  #          for the same reason as previsouly 
  #          => date end BF = BF start date + 13 months AND still breastfed at 
  #             13 months flag
  # Step6: we look at catchup questionnaire end of breastfeeding age question. 
  #        If both age for exclusive and non exclusive bresatfeeding are 
  #        given, priority is given to the non exclusive bresatfeeding as it 
  #        can only be longer. 
  #        * If age of end of non exclusive brestfeeding not missing 
  #          => end of BF date = BF start date + n months non exclusive
  #        * Else if age of end of exclusive brestfeeding not missing 
  #          => end of BF date = BF start date + n months exclusive
  #        * For 2 cases exclusive breastfeeding > non exclusive, the longest 
  #          date (ie exclusive) was used as ("Vu les temps cours indiqués pour 
  #          l’allaitement mixte par rapport à exclusif on peut imaginer que 
  #          c’est des femmes pour qui l’allaitement s’est mis en place 
  #          difficilement et qui ont du donner du lait infantile à leurs enfant 
  #          en début de vie. Je prendrais donc la durée d'allaitement exclusif 
  #          comme durée d’allaitement." e-mail C Philippat 10/05/2021) 
  #          => end of BF date = BF start date + n months exclusive
  
  bf_data <- bf_data %>%
    mutate(
      # * Step 0 ----
      # init start and end dates
      bf_start_date = po_datedel,
      bf_end_date   = ymd(NA),
      
      # * Step 1 ----
      # never breastfed
      # 26 cases
      bf_end_date = if_else(
        !is.na(cy1ebb1_q01) & cy1ebb1_q01 == "0", 
        bf_start_date, 
        bf_end_date
      ),
      
      # * Step 2 ----
      # end of breastfeeding before 12 months declared as date
      # 71 cases
      bf_end_date = case_when(
        is.na(bf_end_date) & !is.na(cy1ebb1_q01p3) ~ cy1ebb1_q01p3, 
        is.na(bf_end_date) & !is.na(cy1ebb2_q01p3) ~ cy1ebb2_q01p3, 
        TRUE ~ bf_end_date
      ),
      
      # * Step 3 ----
      # end of breastfeeding before 12 months declared as age in months and/or weeks
      # 267 cases
      bf_end_date = case_when(
        is.na(bf_end_date) & (!is.na(cy1ebb1_q01p1) | !is.na(cy1ebb1_q01p2)) ~ bf_start_date + months(cy1ebb1_q01p1) + weeks(cy1ebb1_q01p2),
        is.na(bf_end_date) & (!is.na(cy1ebb2_q01p1) | !is.na(cy1ebb2_q01p2)) ~ bf_start_date + months(cy1ebb2_q01p1) + weeks(cy1ebb2_q01p2),
        TRUE ~ bf_end_date
      ),
      
      # * Step 4 ----
      # still breastfed at 12 months
      # 70 cases
      bf_end_date = if_else(
        is.na(bf_end_date) & !is.na(cy1ebb2_q01p4) == 1, 
        bf_start_date + weeks(n_weeks + 1), 
        bf_end_date
      ),
      
      # * Step 5 ---- 
      # catch up questionnaire, still breastfed question
      # 4 cases
      bf_end_date = case_when(
        is.na(bf_end_date) & !is.na(cy1lab1_q01p4) & is.na(cy1lab1_q01p6) ~ bf_start_date + weeks(n_weeks + 1),
        is.na(bf_end_date) & !is.na(cy1lab1_q01p1) & is.na(cy1lab1_q01p3) ~ bf_start_date + weeks(n_weeks + 1),
        TRUE ~ bf_end_date
      ),
      
      # * Step 6 ----
      # catch up questionnaire, end of breastfeeding age
      # 6 cases
      bf_end_date = case_when(
        is.na(bf_end_date) & !is.na(cy1lab1_q01p3) & !is.na(cy1lab1_q01p6) & cy1lab1_q01p3 > cy1lab1_q01p6 ~ bf_start_date + months(cy1lab1_q01p3), 
        is.na(bf_end_date) & (!is.na(cy1lab1_q01p6)) ~ bf_start_date + months(cy1lab1_q01p6), 
        is.na(bf_end_date) & (!is.na(cy1lab1_q01p3)) ~ bf_start_date + months(cy1lab1_q01p3), 
        TRUE ~ bf_end_date
      ),
      
      # * Final breasfeeding duration variable ----
      bf_duration = as.numeric(difftime(bf_end_date, bf_start_date, unit = "weeks"))
    ) %>%
    rowwise() %>%
    mutate(
      # * Set still BF at 48 weeks indicator ----
      still_bf_w48 = ifelse(bf_duration > n_weeks, "1", "0"),
      # * Limit max duration to n_months ----
      bf_duration  = min(bf_duration, n_weeks)
    )
  
  # * Final manual correction ----
  # Two individuals completed their 12 month questionnaire before 48 weeks
  # * ident == 22720 filled it at 40 weeks but declared finishing breastfeeding 
  #   at 17 weeks ==> NO PB
  # * ident == 16490 filled it at 30 weeks, was still breastfeeding at the time, 
  #   and we have no other indication in the following questionnaires on her 
  #   breastfeeding end date 
  # email Claire from 12/05/21 regarding this case
  # "Manquant ne me semble pas la bonne option sachant qu’on sait qu’elle à
  # allaité jusqu’à 30 semaine. L’option serait de lui attribuer le mediane de
  # la durée d’allaitement de celles qui déclaraient allaiter jusqu’à 30
  # semaine."
  bf_data$bf_duration[bf_data$ident == "16490"]  <- median(bf_data$bf_duration[bf_data$bf_duration>30], na.rm = TRUE) # 47.5 weeks
  bf_data$still_bf_w48[bf_data$ident == "16490"] <- "0"
  
  # NOTE use questionnaires cy2eaq9 and cy3ebe1 to see if they were breastfed 
  #        between 12 and 18 months and between 18 months and 24 months
  #        If breastfed between 18 and 24 months: end date = birth date + 2 years
  #        If brestfed between 12 months and 18 months and not between 18 and 24 
  #        months: end date = birth date + 1.5 years

  # export vars
  bf_final <- bf_data %>% select(ident, bf_duration, still_bf_w48)
  
  return(bf_final)
}

# File created for breastfeeding data correction
# # Export cases where bf duration > 2 years or < 0
# flagged_data <- bf_data %>%
#   filter(bf_duration < 0 | bf_duration > 731) %>%
#   select(
#     ident, bf_duration, bf_start_date, bf_end_date, cy1ebb1_q01, cy1ebb1_q01p3,
#     cy1ebb2_q01p3, cy1ebb1_q01p1, cy1ebb1_q01p2, cy1lab1_q01p3, cy2eaq9_q02,
#     cy3ebe1_q37p1
#     )
# write_csv(flagged_data, "results/flagged_bf_data.csv")